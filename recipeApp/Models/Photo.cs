using System;
using Microsoft.AspNetCore.Http;

namespace recipeApp.Models
{
    public class FileUpload
    {
        public Guid id { get; set;}
        //public IFormFile files { get; set; }
        public string name { get; set; }
        public string FileType { get; set; }
        public byte[] DataFiles { get; set; }
        public Guid recipeId { get; set; }
    }
}