using System;

namespace recipeApp.Models{
    public class Process
    {
        public Guid Id { get; set; }
        public Guid recipeId { get; set; }
        public string process { get; set; }
    }
}