using System;

namespace recipeApp.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string fName { get; set; }
        public string lName { get; set; }
        public string pWord { get; set; }
        public string email { get; set; }
        public string jwt { get; set; }
    }
}