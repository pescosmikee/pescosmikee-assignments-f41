using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace recipeApp.Models
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;
        public DataContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var connection = Configuration.GetConnectionString("DefaultConnection");
            options.UseMySql(connection);
        }

        public DbSet<User> users { get; set; }
        public DbSet<Recipe> recipe { get; set; }
        public DbSet<Process> process { get; set;}
        public DbSet<Ingredient> ingredient { get; set;}
        public DbSet<FileUpload> img { get; set;}
    }
}