using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace recipeApp.Models{
    public class Recipe
    {
        public Guid Id { get; set; }
        public Guid by { get; set; }
        public string recipeName { get; set; }
        public List<Ingredient> Ingredient { get;set; }
        public List<Process> Process { get;set; }
        public string note { get; set; }
        //public IFormFile Image { get; set; }
    }
}