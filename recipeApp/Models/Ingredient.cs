using System;

namespace recipeApp.Models{
    public class Ingredient
    {
        public Guid Id { get; set; }
        public Guid recipeId { get; set; }
        public string recipeIngredients { get; set; }
        public string measurement { get; set; }
    }
}