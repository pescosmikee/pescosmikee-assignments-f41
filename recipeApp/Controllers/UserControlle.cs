using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using recipeApp.Models;
using recipeApp.Services;

namespace recipeApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private IUserServices _userServices;
        public UserController(IUserServices userServices )
        {
            _userServices = userServices;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login([FromBody]User model)
        {
            var user = _userServices.Login(model.email, model.pWord);

            if(user == null)
                return BadRequest(new { message = "Error"});
                
            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]User model)
        {
            var user = _userServices.Register(model);
            // create user
            return Ok();
        }
    }
}
