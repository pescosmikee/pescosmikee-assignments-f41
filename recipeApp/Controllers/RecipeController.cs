using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using recipeApp.Models;

namespace recipeApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RecipeController : ControllerBase
    {
        private DataContext _context;

        private readonly IWebHostEnvironment _environment;

        public RecipeController( DataContext context)
        {
            _context = context;
        }
        
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost()]
        public async Task<Recipe> AddRecipe([FromBody]Recipe recipe)
        {
            var curUser = new Guid (User.Identity.Name);
            recipe.by = curUser;
            recipe.Id = Guid.NewGuid();
            recipe.Ingredient = new List<Ingredient>();
            recipe.Process = new List<Process>();
            _context.recipe.Add(recipe);
            _context.SaveChanges();
            return recipe;
        }

        //[HttpPost("{recipeid}/addpic")]
        //public async Task<RecipePicture> AddRecipePic(Guid recipeid, [FromBody]PictureViewModel recipe)
        //{
        
         //   return recipe;
        //}

        [HttpPost("{recipeid}/ingredients")]
        public async Task<Ingredient> AddIngredients(Guid recipeid, [FromBody]Ingredient ingredient)
        {
            ingredient.Id = Guid.NewGuid();
            ingredient.recipeId = recipeid;
            _context.ingredient.Add(ingredient);
            _context.SaveChanges();
            return ingredient;
        }

        [HttpGet("{recipeid}")]
        public ActionResult GetQuestion(Guid recipeid)
        {
            var query = from q in _context.recipe
                        //join user in _context.users on q.CreatedBy equals user.Id
                        where  q.Id == recipeid
                        select new {
                            Id = q.Id,
                            Name = q.recipeName,
                            Eng = q.Ingredient,
                            Process = q.Process
                        };
            if (query == null) return NotFound();

            return new JsonResult(query);
        }

        [HttpPost("{recipeid}/process")]
        public async Task<Process> AddProcess(Guid recipeid, [FromBody]Process process)
        {
            process.Id = Guid.NewGuid();
            process.recipeId = recipeid;
            _context.process.Add(process);
            _context.SaveChanges();
            return process;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet()]
        public IEnumerable getRecipe(){
             var query = from q in _context.recipe
             select new {
                    Id = q.Id,
                    Recipe = q.recipeName,
                    Ingredient = q.Ingredient,
                    Process = q.Process,
                    Note = q.note
                    };
            return query;
        }

        /* [HttpPost("{id}/image")]
        public async Task<FileUpload> AddImage([FromBody]FileUpload fileUpload, Guid recipeid)
        {
            //Getting FileName
            var fileName = Path.GetFileName(files.FileName);
            //Getting file Extension
            var fileExtension = Path.GetExtension(fileName);
            // concatenating  FileName + FileExtension
            var newFileName = String.Concat(Convert.ToString(Guid.NewGuid()), fileExtension);

 
            fileUpload.id = new Guid();  
            fileUpload.name= newFileName;
            fileUpload.FileType = fileExtension;
            fileUpload.recipeId = recipeid;
            
            using (var target = new MemoryStream())
            {
                files.CopyTo(target);
                fileUpload.DataFiles = target.ToArray();
            }

            _context.img.Add(fileUpload);
            _context.SaveChanges();

            return fileUpload;
        } */
    }
}
