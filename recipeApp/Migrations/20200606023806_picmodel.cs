﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace recipeApp.Migrations
{
    public partial class picmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "pic",
                table: "recipe",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "pic",
                table: "recipe");
        }
    }
}
