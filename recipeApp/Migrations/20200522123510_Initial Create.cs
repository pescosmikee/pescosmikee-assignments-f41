﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace recipeApp.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "recipe",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    recipeName = table.Column<string>(nullable: true),
                    note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_recipe", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    fName = table.Column<string>(nullable: true),
                    lName = table.Column<string>(nullable: true),
                    pWord = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    jwt = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ingredient",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    recipeId = table.Column<Guid>(nullable: false),
                    recipeIngredients = table.Column<string>(nullable: true),
                    measurement = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ingredient", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ingredient_recipe_recipeId",
                        column: x => x.recipeId,
                        principalTable: "recipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "process",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    recipeId = table.Column<Guid>(nullable: false),
                    process = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_process", x => x.Id);
                    table.ForeignKey(
                        name: "FK_process_recipe_recipeId",
                        column: x => x.recipeId,
                        principalTable: "recipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ingredient_recipeId",
                table: "ingredient",
                column: "recipeId");

            migrationBuilder.CreateIndex(
                name: "IX_process_recipeId",
                table: "process",
                column: "recipeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ingredient");

            migrationBuilder.DropTable(
                name: "process");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "recipe");
        }
    }
}
