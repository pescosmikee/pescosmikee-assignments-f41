﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace recipeApp.Migrations
{
    public partial class PictureModelAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "recipepic",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    recipeId = table.Column<Guid>(nullable: false),
                    pic = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_recipepic", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "recipepic");
        }
    }
}
